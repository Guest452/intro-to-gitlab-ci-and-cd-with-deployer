<?php
namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'martin_svec');

// Project repository
set('repository', 'git@gitlab.com:Guest452/intro-to-gitlab-ci-and-cd-with-deployer.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys 
//set('shared_files', []);
//set('shared_dirs', []);

// Writable dirs by web server 
//set('writable_dirs', []);
set('allow_anonymous_stats', false);

// Hosts

//host('46.101.229.238')
//    ->user('skoleni')
//    ->identityFile()
//    ->set('deploy_path', '/home/skoleni/martin_svec');

host('185.88.73.5')
    ->user('pixie')
    ->port(64232)
    ->configFile('~/.ssh/config')
    ->identityFile('~/.ssh/id_rsa')
    ->forwardAgent(true)
    ->multiplexing(true)
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->set('deploy_path', '/tmp/aaatest');

// Tasks

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:update_code',
]);

// [Optional] If deploy fails automatically unlock.
//after('deploy:failed', 'deploy:unlock');
