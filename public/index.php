<?php
// test
use LexiSoft\CiCD\MyClass;

require __DIR__ . '/../vendor/autoload.php';

MyClass::shout('Hello world!');
//MyClass::shout(null);
// MyClass::shout('Hello world!');

$database = getenv('MYSQL_DATABASE') ?? '';
$password = getenv('MYSQL_PASSWORD') ?? '';
$host = getenv('MYSQL_HOST') ?? '';
$user = getenv('MYSQL_USER') ?? '';

$pdo = new PDO("mysql:dbname=$database;host=$host", $user, $password);
